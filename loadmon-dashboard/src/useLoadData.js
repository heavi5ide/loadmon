import { useState, useEffect } from "react";

import { getLoad } from "./api";
import { analyzeLoadTimeSeries } from "./loadAnalysis";

import {
  SAMPLES_PER_MINUTE,
  MAX_SAMPLE_COUNT
} from "./constants";

/**
 * A custom hook that polls the API serving up the CPU load metric. This hook
 * function returns an object with the following data:
 * - loadTimeSeries: An array of numbers representing time series data of the
 *   CPU load. The last item is the most recent measurement ("now").
 * - currentLoad: The current CPU (most recent value returned from the API.)
 */
export default function useLoadData() {
  const [loadTimeSeries, setLoadTimeSeries] = useState([]);

  // On mount, we start polling. Each time we poll, we update loadTimeSeries
  useEffect(() => {
    let loadBuffer = [];

    const poll = async () => {
      const load = await getLoad();
      loadBuffer = [...loadBuffer, load];

      // Limit to our max history length
      if (loadBuffer.length > MAX_SAMPLE_COUNT) {
        loadBuffer.shift();
      }

      setLoadTimeSeries(loadBuffer);
    };

    // Start polling
    const interval = setInterval(poll, 60000 / SAMPLES_PER_MINUTE);
    poll();

    // Stop polling on unmount
    return () => {
      clearInterval(interval);
    };
  }, []);

  // currentLoad is just the last sample value in the time series
  const currentLoad = loadTimeSeries.length
    ? loadTimeSeries[loadTimeSeries.length - 1]
    : null;

  const { loadAlerts, currentlyUnderHighLoad } = analyzeLoadTimeSeries(
    loadTimeSeries
  );

  return {
    loadTimeSeries,
    currentLoad,
    loadAlerts,
    currentlyUnderHighLoad
  };
}
