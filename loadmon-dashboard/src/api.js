// This is only set up for development currently; the proxy setting we set in
// package.json tells webpack dev server to proxy requests to our
// loadmon-server, which you must be running.
const loadEndpoint = `/api/load`;

export async function getLoad() {
  const response = await fetch(loadEndpoint);
  const json = await response.json();
  return json.load;
}
