import { HIGH, NOT_HIGH, UNDER_HIGH_LOAD, RECOVERED } from './constants';
import { getExtents, analyzeLoadTimeSeries } from './loadAnalysis';

it('correctly calculates extents of empty time series', () => {
  expect(getExtents([])).toEqual([]);
});

it('correctly calculates one extent', () => {
  expect(getExtents([0, 0, 0])).toEqual([
    {
      start: 0,
      end: 2,
      type: NOT_HIGH
    }
  ]);
});

it('correctly calculates single sample extents', () => {
  expect(getExtents([0, 1.5, 0])).toEqual([
    {
      start: 0,
      end: 0,
      type: NOT_HIGH
    },
    {
      start: 1,
      end: 1,
      type: HIGH
    },
    {
      start: 2,
      end: 2,
      type: NOT_HIGH
    }
  ]);
});

it('correctly calculates longer extents', () => {
  expect(getExtents([0, 0, 0, 1.5, 0, 0])).toEqual([
    {
      start: 0,
      end: 2,
      type: NOT_HIGH
    },
    {
      start: 3,
      end: 3,
      type: HIGH
    },
    {
      start: 4,
      end: 5,
      type: NOT_HIGH
    }
  ]);
});

it('correctly doesn\'t alert on extent of 11 HIGH samples', () => {
  const high11 = [2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2];
  expect(getExtents(high11)).toEqual([
    {
      start: 0,
      end: 10,
      type: HIGH
    }
  ]);

  expect(analyzeLoadTimeSeries(high11)).toEqual({
    loadAlerts: [],
    currentlyUnderHighLoad: false
  });
});

it('correctly alerts on extent of 12 HIGH samples', () => {
  const high12 = [2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2];
  expect(getExtents(high12)).toEqual([
    {
      start: 0,
      end: 11,
      type: HIGH
    }
  ]);

  expect(analyzeLoadTimeSeries(high12)).toEqual({
    loadAlerts: [{
      type: UNDER_HIGH_LOAD,
      index: 11
    }],
    currentlyUnderHighLoad: true
  });
});

it('correctly alerts recovery on extent of 12 NOT_HIGH samples', () => {
  const timeSeries = [2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
                      0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
  expect(getExtents(timeSeries)).toEqual([
    {
      start: 0,
      end: 11,
      type: HIGH
    },
    {
      start: 12,
      end: 23,
      type: NOT_HIGH
    }
  ]);

  expect(analyzeLoadTimeSeries(timeSeries)).toEqual({
    loadAlerts: [
      {
        type: UNDER_HIGH_LOAD,
        index: 11  
      },
      {
        type: RECOVERED,
        index: 23
      }
    ],
    currentlyUnderHighLoad: false
  });
});

it('correctly doesn\'t alert recovery unless we were under high load', () => {
  const timeSeries = [2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 0,
                      0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
  expect(getExtents(timeSeries)).toEqual([
    {
      start: 0,
      end: 10,
      type: HIGH
    },
    {
      start: 11,
      end: 23,
      type: NOT_HIGH
    }
  ]);

  expect(analyzeLoadTimeSeries(timeSeries)).toEqual({
    loadAlerts: [],
    currentlyUnderHighLoad: false
  });
});

it('correctly doesn\'t alert recovery until extent of 12 NOT_HIGH samples', () => {
  const timeSeries = [2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
                      0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2,
                      0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
  expect(getExtents(timeSeries)).toEqual([
    {
      start: 0,
      end: 11,
      type: HIGH
    },
    {
      start: 12,
      end: 22,
      type: NOT_HIGH
    },
    {
      start: 23,
      end: 23,
      type: HIGH
    },
    {
      start: 24,
      end: 35,
      type: NOT_HIGH
    }
  ]);

  expect(analyzeLoadTimeSeries(timeSeries)).toEqual({
    loadAlerts: [
      {
        type: UNDER_HIGH_LOAD,
        index: 11
      },
      {
        type: RECOVERED,
        index: 35
      }
    ],
    currentlyUnderHighLoad: false
  });
});