import React from "react";
import "./App.css";

import useLoadData from "./useLoadData";

import TimeSeriesGraph from "./TimeSeriesGraph";

function App() {
  const {
    loadTimeSeries,
    currentLoad,
    loadAlerts,
    currentlyUnderHighLoad
  } = useLoadData();

  return (
    <div className="App">
      <div className="App-left">
        <TimeSeriesGraph
          loadTimeSeries={loadTimeSeries}
          loadAlerts={loadAlerts}
        />
      </div>
      <div className={"App-right" + (currentlyUnderHighLoad ? " App-highLoad" : "")}>
        <div className="App-loadLabel">Current Load</div>
        <div className={"App-loadNum"}>
          {currentLoad !== null && currentLoad.toFixed(2)}
        </div>
        <div className="App-alert">
          {currentlyUnderHighLoad ? "🔥" : "👌"}
        </div>
      </div>
    </div>
  );
}

export default App;
