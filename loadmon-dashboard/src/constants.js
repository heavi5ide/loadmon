export const SAMPLES_PER_MINUTE = 6;
export const MINUTES_OF_HISTORY = 10;
export const MAX_SAMPLE_COUNT = SAMPLES_PER_MINUTE * MINUTES_OF_HISTORY + 1;

export const HIGH_LOAD_THRESHOLD = 1;

export const HIGH = "HIGH";
export const NOT_HIGH = "NOT_HIGH";
export const UNDER_HIGH_LOAD = "UNDER_HIGH_LOAD";
export const RECOVERED = "RECOVERED";
