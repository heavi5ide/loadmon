import React from "react";
import {
  VictoryAxis,
  VictoryChart,
  VictoryLabel,
  VictoryLine,
  VictoryScatter
} from "victory";

import { MAX_SAMPLE_COUNT } from "./constants";

// Draw a tick label at each minute of history
const TICK_VALUES = [-60, -54, -48, -42, -36, -30, -24, -18, -12, -6, 0];

function calcTimeLabel(x) {
  if (x === 0) {
    return "Now";
  } else if (x === -6) {
    return `1 minute ago`;
  } else {
    return `${(-1 * x) / 6} minutes ago`;
  }
}

export default function TimeSeriesGraph({ loadTimeSeries, loadAlerts }) {
  const displayTimeSeries = loadTimeSeries.map((load, i) => ({
    x: -1 * loadTimeSeries.length + i + 1,
    y: load
  }));

  const thresholdLineData = [
    {
      x: -1 * MAX_SAMPLE_COUNT + 1,
      y: 1
    },
    {
      x: 0,
      y: 1
    }
  ];

  return (
    <VictoryChart
      domain={{ x: [-1 * MAX_SAMPLE_COUNT + 1, 0] }}
      minDomain={{ y: 0 }}
      domainPadding={{ y: 2 }}
      padding={{ top: 50, left: 50, right: 50, bottom: 100 }}
    >
      <VictoryAxis
        tickValues={TICK_VALUES}
        tickFormat={calcTimeLabel}
        tickLabelComponent={
          <VictoryLabel
            angle={90}
            textAnchor="start"
            verticalAnchor="middle"
            style={{ fontFamily: "sans-serif" }}
          />
        }
      />
      <VictoryAxis
        dependentAxis
        tickLabelComponent={
          <VictoryLabel style={{ fontFamily: "sans-serif" }} />
        }
      />
      <VictoryLine
        data={thresholdLineData}
        style={{ data: { stroke: "red", strokeWidth: 1 } }}
      />
      <VictoryLine
        data={displayTimeSeries}
        style={{ data: { strokeWidth: 2 } }}
      />
      <VictoryScatter
        data={displayTimeSeries}
        style={{
          data: {
            fill: ({ datum }) => (datum.y >= 1 ? "red" : "black")
          }
        }}
      />
    </VictoryChart>
  );
}
