import {
  HIGH_LOAD_THRESHOLD,
  HIGH,
  NOT_HIGH,
  UNDER_HIGH_LOAD,
  RECOVERED
} from "./constants";

/**
 * Given a time series of load samples, categorize each sample as HIGH (over
 * the threshold for high load) or NOT_HIGH, and return an array of extents --
 * blocks of contiguous lowness or highness.
 */
export function getExtents(timeSeries) {
  let extents = [];
  let recentHighIndex = null;
  let recentLowIndex = null;

  // Loop through the time series, looking for transitions from low to high or
  // high to low -- these are the boundaries of the extents.
  timeSeries.forEach((load, i) => {
    const isHigh = load >= HIGH_LOAD_THRESHOLD;
    if (isHigh) {
      // This sample is high; if the previous sample was low, it was the end of
      // a low extent.
      if (recentLowIndex === i - 1) {
        extents.push({
          start: recentHighIndex !== null ? recentHighIndex + 1 : 0,
          end: recentLowIndex,
          type: NOT_HIGH
        });
      }
      recentHighIndex = i;
    } else {
      // This sample is low; if the previous sample was high, it was the end of
      // a high extent.
      if (recentHighIndex === i - 1) {
        extents.push({
          start: recentLowIndex !== null ? recentLowIndex + 1 : 0,
          end: recentHighIndex,
          type: HIGH
        });
      }
      recentLowIndex = i;
    }
  });

  // Add the final extent to the list (it's at the end of the time series so we
  // will not have detected a transition to close it off)
  if (recentHighIndex !== null || recentLowIndex !== null) {
    if (recentLowIndex === null || recentHighIndex > recentLowIndex) {
      extents.push({
        start: recentLowIndex !== null ? recentLowIndex + 1 : 0,
        end: recentHighIndex,
        type: HIGH
      });
    } else {
      extents.push({
        start: recentHighIndex !== null ? recentHighIndex + 1 : 0,
        end: recentLowIndex,
        type: NOT_HIGH
      });
    }
  }

  return extents;
}

/**
 * Analyze the passed in time series to return information about where in the
 * time series we should alert the user -- these are the places where the time
 * series is "under high average load" or has "recovered" from that state,
 * as specified in the assignment.
 */
export function analyzeLoadTimeSeries(timeSeries) {
  const extents = getExtents(timeSeries);

  let loadAlerts = [];
  let isUnderHighLoad = false;

  // Kind of weird criteria for "under high average load" here. To calculate,
  // start from the first extent and in a "not under high average load" state.
  // If you see an extent of 12 or more HIGH samples, we alert on high average
  // load on the 12th HIGH sample. Once we're in the "under high average load"
  // state, we're in that state until we see an extent of 12 or more NOT_HIGH
  // samples in a row.
  extents.forEach(extent => {
    if (!isUnderHighLoad) {
      if (extent.type === HIGH && extent.end - extent.start >= 11) {
        loadAlerts.push({
          index: extent.start + 11,
          type: UNDER_HIGH_LOAD
        });
        isUnderHighLoad = true;
      }
    } else {
      if (extent.type === NOT_HIGH && extent.end - extent.start >= 11) {
        loadAlerts.push({
          index: extent.start + 11,
          type: RECOVERED
        });
        isUnderHighLoad = false;
      }
    }
  });

  return {
    loadAlerts,
    currentlyUnderHighLoad: isUnderHighLoad
  };
}
