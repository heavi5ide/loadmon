# loadmon 🔥

Submission by Nick Moy

## How to run
First, run the server:
```
cd loadmon-server
npm install
npm start
```

Then in a separate terminal, run the frontend dashboard:
```
cd loadmon-dashboard
npm install
npm start
```

## How to run tests
In the `loadmon-dashboard` directory, assuming you've already done `npm install`:
```
npm test
```

## Notes

### On the detection of "Under High Load" and "Recovered" transitions
I interpreted “a CPU is considered under high average load when it has exceeded 1 for 2 minutes or more” as meaning that 12 consecutive samples — 2 minutes of samples since they’re taken at 10 second intervals — had to be >= 1 for us to consider the machine as “under high load.” I think if I squint just right I can interpret it instead as meaning that I could use a sliding window of 12 values averaged together, and if the average is >= 1, consider the machine as “under high load.” That would probably be a better metric to use, but I think my initial reading is what the text actually says, and that’s what I initially spent some time on, so that’s what’s in here!

Similarly, I interpreted “a CPU is considered recovered from high average load when it drops below 1 for 2 minutes or more” as meaning that, once a machine is under high load, it will not be considered recovered until we see 12 consecutive samples with a value < 1. This seems very far from ideal, since just a single sample being >=1 will keep the machine in an “under high load” state, resetting us back to zero in our search for 12 consecutive < 1 samples. A sliding window that averaged over 12 samples would have worked much better here IMO, and would have been easier to implement to boot 😀

I approached implementation of the detection of “under high load” and “recovered” states in a functional way, working with the raw time series data (up to 10 minutes of load sample values) and no other state. This is nice for testability — the analyzeLoadTimeSeries function is a pure function that can be tested without needing to set up any state (see loadAnalysis.test.js) — but has a couple of difficulties that, in retrospect, would make me to move in a different direction:

* In the current iteration, the analyzeLoadTimeSeries function takes only the raw time series — there is no way to indicate if at the beginning of the time series (10 minutes ago) we were already in an “under high load” state. Because the logic for determining “under high load” and “recovered” depends on knowing previous state (this would not be the case if it were a simple sliding window average), this means that weird things will happen in some situations, like an “under high load” state that is maintained by virtue of us not seeing 12 consecutive < 1 samples (as described above) being erroneously lost because we’re not keeping track of what state we were in at the start of the history graph.
* I did detection by first identifying contiguous blocks of either “high” (>= 1) or “low” (< 1) load, and then looping over those blocks, considering blocks of 12 or more samples as places where a transition to/from “under high load” could occur. This made some sense as a functional way of transforming the time series, but a single loop over the data would have been more more efficient.
* Even more efficient would be a stateful approach where each new poll of the API just adds a new data point onto the end of the time series, and we just consider whether that new data point made us transition to/from “under high load,” based on some state that we held onto from the previous poll of the API.

### Re: Overall design and "alerts"
I really didn’t have time to do a nicer layout or implement “alerts” in any nice way. The state of “under high load” is communicated through the 🔥 emoji, and a flashing red background on the right side of the layout that flashes for 10 seconds when you cross into “under high load” territory. The state of “recovered” is not communicated any differently than just not being “under high load” — you see the 👌 emoji instead of the 🔥.

### Random note on the tech
I haven’t actually been hands on with frontend code since about January, so this was my first time trying both React hooks and the Victory graphing library. I don’t think I botched anything with either of them!

### Productionization notes

If I was productionizing this, a few things jump out as immediate considerations:

* Just to beat this dead horse, I think the “under high load” calculation doesn’t make much sense. A two minute sliding average would be better. Looking at what Node’s `os.loadavg` actually returns though, it looks like it already has a good amount of logic built in to give you an average over the recent past — we’re currently using the 1-minute average from that return value, which heavily weights the last minute of load, but `os.loadavg` also returns 5-minute and 15-minute average numbers. Using one of these numbers directly would probably give a better experience, instead of calculating another sliding average over these averages. If this were a production thing, I don't think I'd be calculating "under high load" in the frontend at all — the calculations would instead happen in the server process, which could return a timeseries of the previous 10 minutes of data to the frontend, with annotations of when the machine was under high load.
* I’d like to indicate on the graph the ranges of time where we were in the “under high load” state. Currently I draw a horizontal red line at load value `1` and color the data points at or above the line red, but that was just for expediency; it doesn’t actually correspond to the ranges where we were “under high load.”
* The layout and design (I used the CSS `red` which is pretty garish) is super quick and dirty. It’s not completely terrible, but it’s not very nice either.
* The alerting behavior is similarly quick and dirty. I do kind of like the 🔥 emoji to signify "under high load" but currently I'm not actually signaling "recovered" as an event distinct from just not being under high load.
* The only tests I added were for `loadAnalysis.js`. For a production project we would want better coverage.