const express = require("express");
const app = express();
const port = 8091;

const os = require("os");
const cpuCount = os.cpus().length;

app.get("/api/load", (req, res) => {
  const load = os.loadavg()[0] / cpuCount;
  res.json({ load });
});

app.listen(port, () => {
  console.log(`loadmon-server listening on port ${port}!`);
});
