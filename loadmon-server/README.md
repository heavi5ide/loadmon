# loadmon-server

A simple Express-based API server that returns CPU load information.

Start the server with `npm start` and the API will be available at `http://localhost:8091/api/load`